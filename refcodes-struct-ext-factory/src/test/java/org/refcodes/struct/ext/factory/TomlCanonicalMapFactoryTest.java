// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;

public class TomlCanonicalMapFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void tesMarshalUnmarshal() throws MarshalException, UnmarshalException {
		final TomlCanonicalMapFactory theFactory = new TomlCanonicalMapFactory();
		final CanonicalMapBuilder theDataStructure = new CanonicalMapBuilderImpl();
		theDataStructure.put( "aaa/aaa_111", "aaa_111" );
		theDataStructure.put( "aaa/aaa_222", "aaa_222" );
		theDataStructure.put( "aaa/aaa_333", "aaa_333" );
		theDataStructure.put( "bbb/bbb_111", "bbb_111" );
		theDataStructure.put( "bbb/bbb_222", "bbb_222" );
		theDataStructure.put( "bbb/bbb_333", "bbb_333" );
		theDataStructure.put( "ccc/ccc_111", "ccc_111" );
		theDataStructure.put( "ccc/ccc_222", "ccc_222" );
		theDataStructure.put( "ccc/ccc_333", "ccc_333" );
		final Map<String, String> theProperties = new HashMap<>();
		theProperties.put( "comment", "Hello world, this is a comment!" );
		final String theMarshaled = theFactory.toMarshaled( theDataStructure, theProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final CanonicalMapBuilder theUnmarshaled = theFactory.toUnmarshaled( theMarshaled );
		assertEquals( theDataStructure.size(), theUnmarshaled.size() );
		for ( String eKey : theDataStructure.keySet() ) {
			assertEquals( theDataStructure.get( eKey ), theUnmarshaled.get( eKey ) );
		}
	}

	@Disabled
	@Test
	public void tesEdgeCase() throws MarshalException, UnmarshalException, IOException {
		final TomlCanonicalMapFactory theFactory = new TomlCanonicalMapFactory();
		final CanonicalMapBuilder theBuilder = theFactory.fromMarshaled( getClass().getResourceAsStream( "/test.ini" ) );
		for ( String eKey : theBuilder.sortedKeys() ) {
			final String theResult = eKey + "=" + theBuilder.get( eKey );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theResult );
			}
		}
	}
}
