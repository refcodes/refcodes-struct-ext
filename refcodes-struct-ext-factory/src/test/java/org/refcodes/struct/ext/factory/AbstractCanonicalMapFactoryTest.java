// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMapImpl;

public class AbstractCanonicalMapFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// PERSON:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		private String firstName;
		private String lastName;
		private Person child = null;
		private Car car = null;

		public Car getCar() {
			return car;
		}

		public void setCar( Car car ) {
			this.car = car;
		}

		public Person getChild() {
			return child;
		}

		public void setChild( Person child ) {
			this.child = child;
		}

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( car == null ) ? 0 : car.hashCode() );
			result = prime * result + ( ( child == null ) ? 0 : child.hashCode() );
			result = prime * result + ( ( firstName == null ) ? 0 : firstName.hashCode() );
			result = prime * result + ( ( lastName == null ) ? 0 : lastName.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Person other = (Person) obj;
			if ( car == null ) {
				if ( other.car != null ) {
					return false;
				}
			}
			else if ( !car.equals( other.car ) ) {
				return false;
			}
			if ( child == null ) {
				if ( other.child != null ) {
					return false;
				}
			}
			else if ( !child.equals( other.child ) ) {
				return false;
			}
			if ( firstName == null ) {
				if ( other.firstName != null ) {
					return false;
				}
			}
			else if ( !firstName.equals( other.firstName ) ) {
				return false;
			}
			if ( lastName == null ) {
				if ( other.lastName != null ) {
					return false;
				}
			}
			else if ( !lastName.equals( other.lastName ) ) {
				return false;
			}
			return true;
		}
	}

	public static class Car {
		private String _vendor;
		private String _model;
		private Map<String, Object> _properties = new HashMap<>();

		public Car() {}

		public Car( String vendor, String model ) {
			this._vendor = vendor;
			this._model = model;
			_properties.put( "length", 2 );
			_properties.put( "height", 1 );
			final Integer[] values = new Integer[] { 0, 1, 2, 3, 4 };
			_properties.put( "counter", Arrays.asList( values ) );
			_properties.put( "clone", Arrays.asList( values ) );
		}

		public String getVendor() {
			return _vendor;
		}

		public void setVendor( String vendor ) {
			this._vendor = vendor;
		}

		public String getModel() {
			return _model;
		}

		public void setModel( String model ) {
			this._model = model;
		}

		public Map<String, Object> getProperties() {
			return _properties;
		}

		public void setProperties( Map<String, Object> properties ) {
			this._properties = properties;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( _model == null ) ? 0 : _model.hashCode() );
			result = prime * result + ( ( _vendor == null ) ? 0 : _vendor.hashCode() );
			result = prime * result + ( ( _properties == null ) ? 0 : _properties.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Car other = (Car) obj;
			if ( _model == null ) {
				if ( other._model != null ) {
					return false;
				}
			}
			else if ( !_model.equals( other._model ) ) {
				return false;
			}
			if ( _vendor == null ) {
				if ( other._vendor != null ) {
					return false;
				}
			}
			else if ( !_vendor.equals( other._vendor ) ) {
				return false;
			}
			if ( _properties == null ) {
				if ( other._properties != null ) {
					return false;
				}
			}
			else {
				final CanonicalMap theMap = new CanonicalMapImpl( _properties );
				final CanonicalMap theOtherMap = new CanonicalMapImpl( other._properties );
				if ( !theMap.isEqualTo( theOtherMap ) ) {
					return false;
				}
			}
			return true;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// ROBOT:
	// /////////////////////////////////////////////////////////////////////////

	public static class Robot {
		private String firstName;
		private String lastName;
		private Dog dog;

		public Robot() {};

		public Robot( String firstName, String lastName, String dogFirst, String dogLast ) {
			this.firstName = firstName;
			this.lastName = lastName;
			dog = new Dog( dogFirst, dogLast );
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		public Dog getDog() {
			return dog;
		}

		public void setDog( Dog dog ) {
			this.dog = dog;
		}
	}

	public static class Dog {
		private String firstName;
		private String lastName;
		private Map<String, Object> properties = new HashMap<>();

		public Dog() {}

		public Dog( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
			properties.put( "length", 2 );
			properties.put( "height", 1 );
			final Integer[] values = new Integer[] { 0, 1, 2, 3, 4 };
			properties.put( "counter", values );
			properties.put( "clone", values );
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		public Map<String, Object> getProperties() {
			return properties;
		}

		public void setProperties( Map<String, Object> properties ) {
			this.properties = properties;
		}
	}
}
