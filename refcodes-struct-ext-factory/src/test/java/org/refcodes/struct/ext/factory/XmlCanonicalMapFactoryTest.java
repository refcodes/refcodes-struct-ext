// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.CanonicalMapImpl;
import org.refcodes.struct.ext.factory.AbstractCanonicalMapFactoryTest.Car;
import org.refcodes.struct.ext.factory.AbstractCanonicalMapFactoryTest.Person;

public class XmlCanonicalMapFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void tesMarshalUnmarshal() throws MarshalException, UnmarshalException {
		final XmlCanonicalMapFactory theFactory = new XmlCanonicalMapFactory();
		final CanonicalMapBuilder theDataStructure = new CanonicalMapBuilderImpl();
		theDataStructure.put( "root/aaa/aaa_111", "aaa_111" );
		theDataStructure.put( "root/aaa/aaa_222", "aaa_222" );
		theDataStructure.put( "root/aaa/aaa_333", "aaa_333" );
		theDataStructure.put( "root/bbb/bbb_111", "bbb_111" );
		theDataStructure.put( "root/bbb/bbb_222", "bbb_222" );
		theDataStructure.put( "root/bbb/bbb_333", "bbb_333" );
		theDataStructure.put( "root/ccc/ccc_111", "ccc_111" );
		theDataStructure.put( "root/ccc/ccc_222", "ccc_222" );
		theDataStructure.put( "root/ccc/ccc_333", "ccc_333" );
		final Map<String, String> theProperties = new HashMap<>();
		theProperties.put( "comment", "Hello world, this is a comment!" );
		final String theMarshaled = theFactory.toMarshaled( theDataStructure, theProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final CanonicalMapBuilder theUnmarshaled = theFactory.toUnmarshaled( theMarshaled );
		assertEquals( theDataStructure.size(), theUnmarshaled.size() );
		for ( String eKey : theDataStructure.sortedKeys() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + "=" + theUnmarshaled.get( eKey ) );
			}
			assertEquals( theDataStructure.get( eKey ), theUnmarshaled.get( eKey ) );
		}
	}

	@Disabled("For debugging purposes only")
	@Test
	public void testToXml() throws UnmarshalException {
		final CanonicalMapBuilder theDataStructure = new CanonicalMapBuilderImpl();
		theDataStructure.put( "html/body/aaa/bbb/@attr1", "1" );
		theDataStructure.put( "html/body/aaa/bbb/@attr2", "2" );
		theDataStructure.put( "html/body/aaa/bbb/@attr3", "3" );
		theDataStructure.put( "html/body/aaa/bbb", "BBB" );
		theDataStructure.put( "html/body/aaa/@attr1", "1" );
		theDataStructure.put( "html/body/aaa/@attr2", "2" );
		theDataStructure.put( "html/body/aaa/@attr3", "3" );
		theDataStructure.put( "html/body/aaa/bbb/ccc/@attr1", "1" );
		theDataStructure.put( "html/body/aaa/bbb/ccc/@attr2", "2" );
		theDataStructure.put( "html/body/aaa/bbb/ccc/@attr3", "3" );
		theDataStructure.put( "html/body/aaa/bbb/ccc/ddd", "DDD" );
		theDataStructure.put( "html/body/aaa/xxx", "BBB" );
		final String theXml = XmlCanonicalMapFactory.toXml( theDataStructure, true );
		System.out.println( theXml );
		final CanonicalMapBuilder theBuilder = HtmlCanonicalMapFactorySingleton.getInstance().toUnmarshaled( theXml );
		for ( String eKey : theBuilder.sortedKeys() ) {
			System.out.println( eKey + " = " + theBuilder.get( eKey ) );
		}
	}

	@Test
	public void testObjectGraph() throws MarshalException, UnmarshalException {
		final XmlCanonicalMapFactory theFactory = new XmlCanonicalMapFactory();
		final Person thePerson = new Person( "Nolan", "Bushnell" );
		final Person theChild = new Person( "Atari", "VCS 2600" );
		thePerson.setChild( theChild );
		final Car theCar = new Car( "Trabant", "Turbo 2000" );
		thePerson.setCar( theCar );
		final CanonicalMap theDataStructure = new CanonicalMapImpl( thePerson );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theDataStructure.sortedKeys() ) {
				System.out.println( eKey + ": " + theDataStructure.get( eKey ) );
			}
		}
		final String theBody = theFactory.toMarshaled( theDataStructure );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBody );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "================================================================================" );
			}
		}
	}

	@Test
	public void tesIndexed() throws MarshalException, UnmarshalException {
		final XmlCanonicalMapFactory theFactory = new XmlCanonicalMapFactory();
		final CanonicalMapBuilder theDataStructure = new CanonicalMapBuilderImpl();
		theDataStructure.put( "root/x/1/aaa_111", "aaa_111" );
		theDataStructure.put( "root/x/1/aaa_222", "aaa_222" );
		theDataStructure.put( "root/x/1/aaa_333", "aaa_333" );
		theDataStructure.put( "root/x/1/aaa_444/0", "X" );
		theDataStructure.put( "root/x/1/aaa_444/1", "A" );
		theDataStructure.put( "root/x/1/aaa_444/2", "B" );
		theDataStructure.put( "root/x/1/aaa_444/3", "C" );
		theDataStructure.put( "root/x/2/bbb_111", "bbb_111" );
		theDataStructure.put( "root/x/2/bbb_222", "bbb_222" );
		theDataStructure.put( "root/x/2/bbb_333", "bbb_333" );
		theDataStructure.put( "root/x/2/bbb_444/0", "X" );
		theDataStructure.put( "root/x/2/bbb_444/1", "A" );
		theDataStructure.put( "root/x/2/bbb_444/2", "B" );
		theDataStructure.put( "root/x/2/bbb_444/3", "C" );
		theDataStructure.put( "root/x/3/ccc_111", "ccc_111" );
		theDataStructure.put( "root/x/3/ccc_222", "ccc_222" );
		theDataStructure.put( "root/x/3/ccc_333", "ccc_333" );
		theDataStructure.put( "root/x/3/ccc_444/0", "X" );
		theDataStructure.put( "root/x/3/ccc_444/1", "A" );
		theDataStructure.put( "root/x/3/ccc_444/2", "B" );
		theDataStructure.put( "root/x/3/ccc_444/3", "C" );
		theDataStructure.put( "root/x/@attrib", "ATTRIB" );
		theDataStructure.put( "root/x/1/aaa_444/@xyz", "XYZ" );
		theDataStructure.put( "root/x/2/bbb_444/@xyz", "XYZ" );
		theDataStructure.put( "root/x/3/ccc_444/@xyz", "XYZ" );
		final Map<String, String> theProperties = new HashMap<>();
		theProperties.put( "comment", "Hello world, this is a comment!" );
		final String theMarshaled = theFactory.toMarshaled( theDataStructure, theProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final CanonicalMapBuilder theUnmarshaled = theFactory.toUnmarshaled( theMarshaled );
		for ( String eKey : theUnmarshaled.sortedKeys() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + "=" + theUnmarshaled.get( eKey ) );
			}
		}
		final String theXml = theFactory.toMarshaled( theUnmarshaled, theProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theXml );
		}
		final String[] theMarshaledArray = theMarshaled.split( "\n" );
		final String[] theXmlArray = theXml.split( "\n" );
		Arrays.sort( theMarshaledArray );
		Arrays.sort( theXmlArray );
		assertArrayEquals( theMarshaledArray, theXmlArray );
	}

	@Test
	public void tesEdgeCase() throws MarshalException, UnmarshalException, IOException {
		final XmlCanonicalMapFactory theFactory = new XmlCanonicalMapFactory();
		final CanonicalMapBuilder theBuilder = theFactory.fromMarshaled( getClass().getResourceAsStream( "/test.xml" ) );
		try ( BufferedReader theReader = new BufferedReader( new InputStreamReader( getClass().getResourceAsStream( "/test.txt" ) ) ) ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				final String theResult = eKey + "=" + theBuilder.get( eKey );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theResult );
				}
				final String theLine = theReader.readLine();
				assertEquals( theLine, theResult );
			}
		}
	}
}
