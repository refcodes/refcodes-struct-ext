module org.refcodes.struct.ext.factory {
	requires com.fasterxml.jackson.core;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.dataformat.yaml;
	requires jakarta.activation;
	requires java.xml;
	requires org.jsoup;
	requires org.refcodes.runtime;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.struct;

	exports org.refcodes.struct.ext.factory;
}
