// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.refcodes.data.Prefix;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;

/**
 * Implementation of the {@link CanonicalMapFactory} for parsing the JSON
 * notation.
 */
public class JavaCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final char COMMENT = '#';

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		final char[] theSupportedDelimiters = DocumentProperty.toDelimiters( aProperties );
		final java.util.Properties theProperties = new java.util.Properties();
		try {
			theProperties.load( aExternalRepresentation );
		}
		catch ( IllegalArgumentException | IOException e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from input stream <" + aExternalRepresentation + ">!", e );
		}
		final CanonicalMapBuilder theResult = new CanonicalMapBuilderImpl( theDelimiter );
		String eKey;
		String eValue;
		for ( Object eObj : theProperties.keySet() ) {
			eKey = (String) eObj;
			eValue = theProperties.getProperty( eKey );
			eKey = theResult.fromExternalPath( eKey, theSupportedDelimiters );
			theResult.put( eKey, eValue );
		}
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return toMarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final char[] theSupportedDelimiters = DocumentProperty.toDelimiters( aProperties );
		final java.util.Properties theProperties = new java.util.Properties();
		String eValue;
		final List<String> theKeys = new ArrayList<>( aDataStructure.sortedKeys() );
		for ( String eKey : theKeys ) {
			eValue = aDataStructure.get( eKey );
			eKey = aDataStructure.toExternalPath( eKey, theSupportedDelimiters[0] );
			eKey = aDataStructure.toPropertyPath( eKey );
			theProperties.put( eKey, eValue == null ? "" : eValue );
		}

		final String theComment = DocumentProperty.toComment( aProperties );
		try {
			final StringWriter theWriter = new StringWriter();
			theProperties.store( new PrintWriter( theWriter ), theComment );
			// Bad hack to fore omitting a heading comment (java.util.Properties always create one) |-->
			String theResult = theWriter.getBuffer().toString();
			if ( theComment == null ) {
				while ( theResult.startsWith( Prefix.JAVA_PROPERTIES_COMMENT.getPrefix() ) ) {
					theResult = theResult.split( "\r\n|\r|\n", 2 )[1];
				}
			}
			// Bad hack to fore omitting a heading comment (java.util.Properties always create one) <--|
			return theResult;
		}
		catch ( IOException e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + CanonicalMap.class.getName() + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return fromUnmarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final String theMarshaled = toMarshaled( aDataStructure, aProperties );
		final InputStream theMarshalledStream = new ByteArrayInputStream( theMarshaled.getBytes() );
		return theMarshalledStream;
	}
}
