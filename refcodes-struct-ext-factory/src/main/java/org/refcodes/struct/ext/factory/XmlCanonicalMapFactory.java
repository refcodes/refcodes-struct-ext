// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Encoding;
import org.refcodes.data.MarshalParameter;
import org.refcodes.data.Prefix;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Implementation of the {@link CanonicalMapFactory} for parsing the XML
 * notation.
 */
public class XmlCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	protected static final String ROOT_SELECTOR = "root";
	public static final String COMMENT_OPEN = "<!--";
	public static final String COMMENT_CLOSE = "-->";
	private static final String XML_TEXT_VALUE = "#text";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		try {
			final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl( theDelimiter );
			final DocumentBuilderFactory theFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder theBuilder = theFactory.newDocumentBuilder();
			final Document theDocument = theBuilder.parse( new InputSource( aExternalRepresentation ) );
			theDocument.normalize();
			final NodeList theElements = theDocument.getChildNodes();
			visitElements( theCanonicalMap, theElements, theCanonicalMap.getRootPath(), theDelimiter );
			postProcess( theCanonicalMap );
			return theCanonicalMap;
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return toMarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc} Any path elements found in the {@link CanonicalMap}
	 * beginning with a "@" as of {@link Prefix#ANNOTATION} are considered being
	 * attributes of their parent path and are marshaled as XML attributes for
	 * the according XML element. Providing values for the
	 * {@link MarshalParameter#CHARSET}, {@link MarshalParameter#COMMENT} or the
	 * {@link MarshalParameter#VERSION} parameter in the provided properties you
	 * can influence the creation of the
	 * <code>&lt;?xml ecnoding="UTF-8" version="1.0" standalone="yes"?&gt;</code>
	 * declaration in the resulting XML.
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		String theRootElement = aProperties != null ? aProperties.get( MarshalParameter.ROOT_ELEMENT.getName() ) : null;
		if ( theRootElement == null || theRootElement.isEmpty() ) {
			theRootElement = ROOT_SELECTOR;
		}

		// Auto-detect whether we need a root element |-->
		if ( aDataStructure.dirs().size() > 1 || ( aDataStructure.dirs().size() == 0 && aDataStructure.leaves().size() != 1 ) || ( aDataStructure.dirs().size() == 1 && aDataStructure.leaves().size() != 0 ) ) {
			aDataStructure = aDataStructure.retrieveTo( theRootElement );
		}
		// Auto-detect whether we need a root element <--|

		// XML document header |-->
		String theHeader;
		if ( aProperties != null && aProperties.containsKey( MarshalParameter.HEADER.getName() ) && aProperties.get( MarshalParameter.HEADER.getName() ) == null ) {
			theHeader = "";
		}
		else {
			theHeader = aProperties != null ? aProperties.get( MarshalParameter.HEADER.getName() ) : null;
		}
		if ( theHeader == null ) {
			String theEncoding = aProperties != null ? aProperties.get( MarshalParameter.ENCODING.getName() ) : null;
			if ( theEncoding == null || theEncoding.isEmpty() ) {
				theEncoding = Encoding.UTF_8.getCode();
			}
			String theVersion = aProperties != null ? aProperties.get( MarshalParameter.VERSION.getName() ) : null;
			if ( theVersion == null || theVersion.isEmpty() ) {
				theVersion = "1.0";
			}
			final boolean isStandAnlone = aProperties != null ? !BooleanLiterals.isFalse( aProperties.get( MarshalParameter.STAND_ALONE.getName() ) ) : true;
			if ( theVersion == null || theVersion.isEmpty() ) {
				theVersion = "1.0";
			}
			theHeader = "<?xml version=\"" + theVersion + "\" encoding=\"" + theEncoding + "\" standalone=\"" + ( isStandAnlone ? "yes" : "no" ) + "\"?>";
		}
		if ( theHeader.length() != 0 ) {
			theHeader += System.lineSeparator();
			// XML document header <--|
		}

		final String theXml = toXml( aDataStructure, DocumentProperty.hasArrayIndex( aProperties ) );

		// Write the comment |-->
		final String theComment = DocumentProperty.toComment( aProperties );
		if ( theComment != null && theComment.length() > 0 ) {
			final String theLines[] = theComment.split( "\\r?\\n" );
			for ( String eLine : theLines ) {
				theHeader += COMMENT_OPEN + " " + eLine + " " + COMMENT_CLOSE + System.lineSeparator();
			}
		}
		// Write the comment <--|
		return theHeader + theXml;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return fromUnmarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final String theMarshaled = toMarshaled( aDataStructure, aProperties );
		final InputStream theMarshalledStream = new ByteArrayInputStream( theMarshaled.getBytes() );
		return theMarshalledStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a {@link CanonicalMap} to XML.
	 * 
	 * @param aCanonicalMap The {@link CanonicalMap} to marshal.
	 * 
	 * @return The resulting XML.
	 */
	protected static String toXml( CanonicalMap aCanonicalMap, boolean hasArrayIndexAttribute ) {
		return toXml( aCanonicalMap, aCanonicalMap.getRootPath(), "", hasArrayIndexAttribute );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postProcess( CanonicalMapBuilder aProperties ) {
		final String theSuffix = aProperties.getDelimiter() + XML_TEXT_VALUE;
		String eToPath;
		String eValue;
		for ( String eKey : new ArrayList<String>( aProperties.keySet() ) ) {
			if ( eKey.endsWith( theSuffix ) ) {
				eToPath = eKey.substring( 0, eKey.length() - theSuffix.length() );
				if ( !aProperties.isLeaf( eToPath ) ) {
					eValue = aProperties.remove( eKey );
					if ( eValue != null ) {
						aProperties.put( eToPath, eValue );
					}

				}

			}
		}
		super.postProcess( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static String toXml( CanonicalMap aCanonicalMap, String aPath, String aIndent, boolean hasArrayIndexAttribute ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( String eChild : aCanonicalMap.children( aPath ) ) {
			final String ePath = aCanonicalMap.toPath( aPath, eChild );
			if ( aCanonicalMap.isArray( ePath ) ) {
				final String theArray = toArrayIndex( aCanonicalMap, aPath, eChild, aIndent, hasArrayIndexAttribute );
				theBuffer.append( theArray );
			}
			else if ( aCanonicalMap.isIndexDir( ePath ) ) {
				final String theArray = toDirIndex( aCanonicalMap, aPath, eChild, aIndent, hasArrayIndexAttribute );
				theBuffer.append( theArray );
			}
			else {
				if ( !eChild.startsWith( aCanonicalMap.getAnnotator() + "" ) ) {
					final String theAttributes = toAttributes( aCanonicalMap, ePath );
					final String eElement = toElement( aCanonicalMap, aPath, eChild, aIndent, theAttributes, hasArrayIndexAttribute );
					theBuffer.append( eElement );
				}
			}
		}
		return theBuffer.toString();
	}

	private static String toElement( CanonicalMap aCanonicalMap, String aPath, String aChild, String aIndent, String aAttribs, boolean hasArrayIndexAttribute ) {
		final StringBuilder theBuffer = new StringBuilder();
		final String thePath = aCanonicalMap.toPath( aPath, aChild );
		theBuffer.append( aIndent );
		theBuffer.append( "<" + aChild + aAttribs );
		String theValue = "";
		if ( aCanonicalMap.containsKey( thePath ) ) {
			theValue = aCanonicalMap.get( thePath );
		}
		final String theContent = toXml( aCanonicalMap, thePath, aIndent + "\t", hasArrayIndexAttribute );
		if ( ( theContent == null || theContent.isEmpty() ) && ( theValue == null || theValue.isEmpty() ) ) {
			// if ( theContent.length() == 0 && theValue.length() == 0 ) {
			theBuffer.append( "/>" );
		}
		else {
			theBuffer.append( ">" );
			theBuffer.append( theValue );
			if ( theContent.length() != 0 ) {
				theBuffer.append( System.lineSeparator() );
				theBuffer.append( theContent );
				theBuffer.append( aIndent );
			}
			theBuffer.append( "</" + aChild + ">" );
			theBuffer.append( System.lineSeparator() );
		}
		return theBuffer.toString();
	}

	private static String toAttributes( CanonicalMap aCanonicalMap, String aPath ) {
		final StringBuilder theBuffer = new StringBuilder();
		String eLeaf;
		String eValue;
		final Set<String> theAttribPaths = aCanonicalMap.queryPaths( aPath, PathMap.ANNOTATOR + "*" );
		for ( String ePath : theAttribPaths ) {
			eValue = aCanonicalMap.get( ePath );
			eLeaf = aCanonicalMap.toLeaf( ePath ).substring( 1 );
			if ( theBuffer.length() != 0 ) {
				theBuffer.append( " " );
			}
			theBuffer.append( eLeaf );
			theBuffer.append( '=' );
			theBuffer.append( '"' );
			theBuffer.append( eValue );
			theBuffer.append( '"' );
		}
		if ( theBuffer.length() != 0 ) {
			theBuffer.insert( 0, ' ' );
		}
		return theBuffer.toString();
	}

	private static String toArrayIndex( CanonicalMap aCanonicalMap, String aPath, String aChild, String aIndent, boolean hasArrayIndexAttribute ) {
		final StringBuilder theBuffer = new StringBuilder();
		final String thePath = aCanonicalMap.toPath( aPath, aChild );
		final int[] indexes = aCanonicalMap.getArrayIndexes( thePath );
		final String theAttribs = toAttributes( aCanonicalMap, thePath );
		String eIndex;
		for ( int i : indexes ) {
			eIndex = toArrayIndex( aCanonicalMap, thePath, i, aChild, theAttribs, aIndent, hasArrayIndexAttribute );
			theBuffer.append( eIndex );
		}
		return theBuffer.toString();
	}

	private static String toArrayIndex( CanonicalMap aCanonicalMap, String aPath, int aIndex, String aElement, String aAttribs, String aIndent, boolean hasArrayIndexAttribute ) {
		final String theAttribs = toAttributes( aCanonicalMap, aCanonicalMap.toPath( aPath, aIndex ) );
		aAttribs += theAttribs;
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( aIndent );
		theBuffer.append( "<" );
		theBuffer.append( aElement );
		theBuffer.append( aAttribs );
		if ( hasArrayIndexAttribute ) {
			theBuffer.append( " " + INDEX_ATTRIBUTE + "=\"" );
			theBuffer.append( aIndex );
			theBuffer.append( "\"" );
		}
		final String theValue = aCanonicalMap.get( aPath, aIndex );
		if ( theValue == null || theValue.isEmpty() ) {
			theBuffer.append( "/>" );
		}
		else {
			theBuffer.append( '>' );
			theBuffer.append( theValue );
			theBuffer.append( "</" );
			theBuffer.append( aElement );
			theBuffer.append( '>' );
		}
		theBuffer.append( System.lineSeparator() );
		return theBuffer.toString();
	}

	private static String toDirIndex( CanonicalMap aCanonicalMap, String aPath, String aChild, String aIndent, boolean hasArrayIndexAttribute ) {
		final StringBuilder theBuffer = new StringBuilder();
		final String thePath = aCanonicalMap.toPath( aPath, aChild );
		final int[] indexes = aCanonicalMap.getDirIndexes( thePath );
		final String theAttribs = toAttributes( aCanonicalMap, thePath );
		String eIndex;
		CanonicalMap eCanonicalMap;
		for ( int i : indexes ) {
			eCanonicalMap = aCanonicalMap.getDirAt( thePath, i );
			eIndex = toDirIndex( eCanonicalMap, thePath, i, aChild, theAttribs, aIndent, hasArrayIndexAttribute );
			theBuffer.append( eIndex );
		}
		return theBuffer.toString();
	}

	private static String toDirIndex( CanonicalMap aCanonicalMap, String aPath, int aIndex, String aElement, String aAttribs, String aIndent, boolean hasArrayIndexAttribute ) {
		final StringBuilder theBuffer = new StringBuilder();
		final String theAttribs = toAttributes( aCanonicalMap, aCanonicalMap.getRootPath() );
		theBuffer.append( aIndent );
		theBuffer.append( "<" );
		theBuffer.append( aElement );
		theBuffer.append( aAttribs );
		theBuffer.append( theAttribs );
		if ( hasArrayIndexAttribute ) {
			theBuffer.append( " " + INDEX_ATTRIBUTE + "=\"" );
			theBuffer.append( aIndex );
			theBuffer.append( "\"" );
		}
		final String theValue = toXml( aCanonicalMap, "", aIndent + "\t", hasArrayIndexAttribute );
		if ( theValue == null || theValue.isEmpty() ) {
			theBuffer.append( "/>" );
		}
		else {
			theBuffer.append( '>' );
			theBuffer.append( System.lineSeparator() );
			theBuffer.append( theValue );
			theBuffer.append( System.lineSeparator() );
			theBuffer.append( aIndent );
			theBuffer.append( "</" );
			theBuffer.append( aElement );
			theBuffer.append( '>' );
		}
		theBuffer.append( System.lineSeparator() );
		return theBuffer.toString();
	}

	private void visitElements( CanonicalMapBuilder aCanonicalMap, NodeList aElements, String aPath, char aDelimiter ) {
		for ( int i = 0; i < aElements.getLength(); i++ ) {
			visitElement( aCanonicalMap, aElements.item( i ), aPath, aDelimiter );
		}
	}

	private void visitElement( CanonicalMapBuilder aCanonicalMap, Node aElement, String aPath, char aDelimiter ) {
		aPath = aCanonicalMap.toPath( aPath, aElement.getNodeName() );
		final int next = toAttributeIndex( aElement.getAttributes() );
		if ( next != -1 ) {
			aPath = aCanonicalMap.toPath( aPath, next );
		}
		aPath = toIndexed( aCanonicalMap, aPath );

		String eValue;
		final NodeList eChildren = aElement.getChildNodes();

		if ( aElement.getNodeType() == Node.TEXT_NODE || aElement.getNodeType() == Node.CDATA_SECTION_NODE ) {
			eValue = aElement.getTextContent();
			if ( eValue != null && eValue.trim().isEmpty() ) {
				eValue = null;
			}
			if ( eValue != null ) {
				aCanonicalMap.add( aPath, eValue );
			}
		}
		visitAttributes( aCanonicalMap, aElement.getAttributes(), aPath );
		visitElements( aCanonicalMap, eChildren, aPath, aDelimiter );
	}

	private void visitAttributes( CanonicalMapBuilder aCanonicalMap, NamedNodeMap aAttribs, String aPath ) {
		if ( aAttribs != null ) {
			Node eAttribute;
			for ( int i = 0; i < aAttribs.getLength(); i++ ) {
				eAttribute = aAttribs.item( i );
				visitAttribute( aCanonicalMap, aPath, eAttribute );
			}
		}
	}

	private void visitAttribute( CanonicalMapBuilder aCanonicalMap, String aPath, Node eAttribute ) {
		if ( !INDEX_ATTRIBUTE.equals( eAttribute.getNodeName() ) ) {
			aPath = aCanonicalMap.toPath( aPath, PathMap.ANNOTATOR + eAttribute.getNodeName() );
			aCanonicalMap.add( aPath, eAttribute.getNodeValue() );
		}
	}

	private int toAttributeIndex( NamedNodeMap aAttribs ) {
		final int index = -1;
		if ( aAttribs != null ) {
			Node eAttribute;
			try {
				for ( int i = 0; i < aAttribs.getLength(); i++ ) {
					eAttribute = aAttribs.item( i );
					if ( INDEX_ATTRIBUTE.equals( eAttribute.getNodeName() ) ) {
						return Integer.parseInt( eAttribute.getNodeValue() );
					}
				}
			}
			catch ( NumberFormatException ignore ) {/* ignore */ }
		}
		return index;
	}

	private String toIndexed( CanonicalMapBuilder aCanonicalMap, String aPath ) {
		if ( aCanonicalMap.isIndexDir( aPath ) ) {
			aPath = aCanonicalMap.toPath( aPath, aCanonicalMap.nextDirIndex( aPath ) );
		}
		else if ( aCanonicalMap.isDir( aPath ) ) {
			final CanonicalMap theSubDirs = aCanonicalMap.removeFrom( aPath );
			aCanonicalMap.insertTo( aCanonicalMap.toPath( aPath, "0" ), theSubDirs );
			aPath = aCanonicalMap.toPath( aPath, "1" );
		}
		return aPath;
	}
}
