// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.util.Map;
import java.util.function.Supplier;

import org.refcodes.data.MarshalParameter;
import org.refcodes.mixin.KeyAccessor;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.PathMap;

/**
 * This enumeration compiles common factory properties related to according
 * documents' notations (XML, JSON, YAML, TOML, PROPERTIES, etc.).
 */
public enum DocumentProperty implements KeyAccessor<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Denoting a property specifying a comment.
	 */
	COMMENT(MarshalParameter.COMMENT.getName(), null),

	/**
	 * Denoting a property specifying the valid delimiter characters all put in
	 * a {@link String}.
	 */
	DELIMITERS(MarshalParameter.DELIMITERS.getName(), null),

	/**
	 * Denoting a property specifying the valid delimiter characters all put in
	 * a {@link String}.
	 */
	DELIMITER(MarshalParameter.DELIMITER.getName(), null),

	/**
	 * 
	 * When set to <code>true</code> then unmarshaling (XML) documents will
	 * preserve the preserve the root element (envelope) even when merely acting
	 * as envelope. As an (XML) document requires a root element, the root
	 * element often is provided merely as of syntactic reasons and must be
	 * omitted as of semantic reasons. Unmarshaling functionality therefore by
	 * default skips the root elelemt, as this is considered merely to serve as
	 * an envelope. This behavior can be overridden by setting this property to
	 * <code>true</code>.
	 */

	ENVELOPE(MarshalParameter.DOCUMENT_ENVELOPE.getName(), SystemProperty.DOCUMENT_ENVELOPE::getValue),

	/**
	 * When set to <code>true</code> then marshaling (XML) documents will
	 * preserve array index information by using an index attribute for array
	 * elements in the produced XML.
	 */
	ARRAY_INDEX(MarshalParameter.DOCUMENT_ARRAY_INDEX.getName(), SystemProperty.DOCUMENT_ARRAY_INDEX::getValue);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _key;
	private Supplier<String> _systemProperty;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private DocumentProperty( String aKey, Supplier<String> aSystemProperty ) {
		_key = aKey;
		_systemProperty = aSystemProperty;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * Determines from the given properties (and if applicable the system
	 * properties) whether the according {@link DocumentProperty} is true.
	 * 
	 * @param aProperties The {@link Map} containing the document properties.
	 * 
	 * @return True in case the {@link DocumentProperty} is determined to e
	 *         true.
	 */
	public boolean isTrue( Map<String, String> aProperties ) {
		return _systemProperty != null && _systemProperty.get() != null && _systemProperty.get().length() != 0 ? Boolean.valueOf( _systemProperty.get() ) : aProperties != null ? Boolean.valueOf( aProperties.get( _key ) ) : false;
	}

	/**
	 * Determines from the given properties (and if applicable the system
	 * properties) the according {@link DocumentProperty} value.
	 * 
	 * @param aProperties The {@link Map} containing the document properties.
	 * 
	 * @return The according value ({@link String}) of the
	 *         {@link DocumentProperty}.
	 */
	public String getValue( Map<String, String> aProperties ) {
		return _systemProperty != null && _systemProperty.get() != null && _systemProperty.get().length() != 0 ? _systemProperty.get() : aProperties != null && aProperties.get( _key ) != null ? aProperties.get( _key ) : null;
	}

	/**
	 * Determines from the given properties (and if applicable the system
	 * properties) the according {@link #DELIMITER} {@link Character}.
	 *
	 * @param aProperties The {@link Map} containing the {@link #DELIMITER}
	 *        property.
	 * @param aDefaultDelimiter The default delimiter to use when none is found
	 *        in the properties.
	 *
	 * @return The according character.
	 */
	public static char toDelimiter( Map<String, String> aProperties, char aDefaultDelimiter ) {
		if ( aProperties != null ) {
			final String theDelimiterString = DELIMITER.getValue( aProperties );
			if ( theDelimiterString != null && theDelimiterString.length() != 0 ) {
				return theDelimiterString.charAt( 0 );
			}
		}
		return aDefaultDelimiter;
	}

	/**
	 * Retrieves the supported delimiters to be used when unmarshaling an
	 * external representation.
	 * 
	 * @param aProperties The properties from which to retrieve the supported
	 *        delimiters. Them delimiters are provided in a {@link String}.
	 * 
	 * @return The according supported delimiters or the
	 *         {@link PathMap#DELIMITER} if none was provided in the properties.
	 */
	public static char[] toDelimiters( Map<String, String> aProperties ) {
		char[] theDelimiters = null;
		if ( aProperties != null ) {
			final String theDelimiterString = DELIMITERS.getValue( aProperties );
			if ( theDelimiterString != null && theDelimiterString.length() != 0 ) {
				theDelimiters = theDelimiterString.toCharArray();
			}
		}
		if ( theDelimiters == null ) {
			theDelimiters = new char[] { CanonicalMap.DELIMITER };
		}
		return theDelimiters;
	}

	/**
	 * Retrieves the comment to be used in the header of the marshaled
	 * representation.
	 * 
	 * @param aProperties The properties from which to retrieve the comment.
	 * 
	 * @return The according {@link DocumentProperty#COMMENT} comment being set.
	 */
	public static String toComment( Map<String, String> aProperties ) {
		return COMMENT.getValue( aProperties );
	}

	/**
	 * Determines whether unmarshaling (XML) documents will preserve the root
	 * element (envelope) even when merely acting as an envelope. As an (XML)
	 * document requires a root element, the root element often is provided
	 * merely as of syntactic reasons and must be omitted as of semantic
	 * reasons.
	 * 
	 * @param aProperties The properties from which to retrieve the envelope
	 *        flag.
	 * 
	 * @return True in case {@link DocumentProperty#ENVELOPE} property is set
	 *         accordingly.
	 */
	public static boolean hasEnvelope( Map<String, String> aProperties ) {
		return ENVELOPE.isTrue( aProperties );
	}

	/**
	 * Determines whether marshaling (XML) documents will preserve array index
	 * information by using an index attribute for array elements in the
	 * produced XML.
	 * 
	 * @param aProperties The properties from which to retrieve the array index
	 *        flag.
	 * 
	 * @return True in case {@link DocumentProperty#ARRAY_INDEX} property is set
	 *         accordingly.
	 */
	public static boolean hasArrayIndex( Map<String, String> aProperties ) {
		return ARRAY_INDEX.isTrue( aProperties );
	}
}
