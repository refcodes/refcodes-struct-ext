// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implementation of the {@link CanonicalMapFactory} for parsing the JSON
 * notation.
 */
public class JsonCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String JSON_TEXT_VALUE = "#text";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		final ObjectMapper theMapper = new ObjectMapper();
		theMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
		try {
			final Map<Object, Object> theProperties = theMapper.readValue( aExternalRepresentation, new TypeReference<Map<Object, Object>>() {} );
			final CanonicalMapBuilder theResult = new CanonicalMapBuilderImpl( theProperties, theDelimiter );
			postProcess( theResult );
			return theResult;
		}
		catch ( JsonMappingException | JsonParseException e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		final ObjectMapper theMapper = new ObjectMapper();
		theMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
		final Object theDataStructure = aDataStructure.toDataStructure();
		try {
			final String theJson = theMapper.writerWithDefaultPrettyPrinter().writeValueAsString( theDataStructure );
			return theJson + System.lineSeparator();
		}
		catch ( IOException e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + CanonicalMap.class.getName() + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		final ObjectMapper theMapper = new ObjectMapper();
		theMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
		final Object theDataStructure = aDataStructure.toDataStructure();
		try {
			final PipedOutputStream theOut = new PipedOutputStream();
			final PipedInputStream theIn = new PipedInputStream( theOut );
			theMapper.writerWithDefaultPrettyPrinter().writeValue( theOut, theDataStructure );
			return theIn;
		}
		catch ( Exception e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + CanonicalMap.class.getName() + ">.", e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postProcess( CanonicalMapBuilder aProperties ) {
		final String theSuffix = aProperties.getDelimiter() + JSON_TEXT_VALUE;
		String eToPath;
		String eValue;
		for ( String eKey : new ArrayList<String>( aProperties.keySet() ) ) {
			if ( eKey.endsWith( theSuffix ) ) {
				eToPath = eKey.substring( 0, eKey.length() - theSuffix.length() );
				if ( !aProperties.isLeaf( eToPath ) ) {
					eValue = aProperties.remove( eKey );
					if ( eValue != null ) {
						aProperties.put( eToPath, eValue );
					}

				}

			}
		}
		super.postProcess( aProperties );
	}
}
