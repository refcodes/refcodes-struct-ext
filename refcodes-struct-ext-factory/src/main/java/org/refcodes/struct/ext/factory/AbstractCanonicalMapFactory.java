// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;

/**
 * Base class for the CanonicalMapFactory providing base functionality.
 */
public abstract class AbstractCanonicalMapFactory implements CanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>index="number"</code> attribute may be
	 */
	static final String INDEX_ATTRIBUTE = "index";

	/**
	 * The names specified in this array denote a key which's value is to be
	 * used for the parent's path's value when parsing a configuration file.
	 * E.g. "yaml" files cannot have a value for a node which has sub-nodes,
	 * same applies to "json" and "xml" which makes using the "mixed content"
	 * mechanism hard. When we require a value for a node (path) which may have
	 * sub-nodes (sub-paths), then e.g. a <code>this</code> sub-node assigns a
	 * value to it's parent node. E.g. For "xml", the attribute
	 * <code>this="value"</code> attached to an element the takes care on giving
	 * an XML element with children an own value.
	 */
	static final String[] THIS_ATTRIBUTES = new String[] { "this", ".", "_" };

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This method takes care on giving nodes with children an own value: E.g.
	 * "yaml" files cannot have a value for a node which has sub-nodes, same
	 * applies to "json" and "xml" which makes using the "mixed content"
	 * mechanism hard. When we require a value for a node (path) which may have
	 * sub-nodes (sub-paths), then e.g. a <code>this</code> sub-node assigns a
	 * value to it's parent node. E.g. For "xml", the attribute
	 * <code>this="value"</code> attached to an element takes care on giving an
	 * XML element with children an own value. See {@link #THIS_ATTRIBUTES}.
	 * 
	 * @param aProperties The properties where the "this" children are to assign
	 *        a value to their parents.
	 */
	protected void postProcess( CanonicalMapBuilder aProperties ) {
		final Set<String> theKeys = new HashSet<>( aProperties.keySet() );
		String ePropertyKey;
		String eValue;
		String ePropertyValue;

		for ( String ePrefix : new String[] { "", aProperties.getAnnotator() + "" } ) {
			for ( String eAttribute : THIS_ATTRIBUTES ) {
				final String theAttributeSuffix = aProperties.getDelimiter() + ePrefix + eAttribute;
				for ( String eKey : theKeys ) {
					if ( eKey.endsWith( theAttributeSuffix ) ) {
						eValue = aProperties.get( eKey );
						if ( eValue != null && eValue.length() != 0 ) {
							ePropertyKey = eKey.substring( 0, eKey.length() - theAttributeSuffix.length() );
							ePropertyValue = aProperties.get( ePropertyKey );
							if ( ePropertyValue != null && ePropertyValue.length() != 0 ) {
								// throw new BugException( "The path <" + ePropertyKey + "> already contains a value <" + ePropertyValue + "> which we wanted to replace with the value <" + eValue + " from the path's attribute <" + THIS_ATTRIBUTES + ">." );
								continue;
							}
							aProperties.put( ePropertyKey, eValue );
							aProperties.remove( eKey );
						}
					}
				}
			}
		}
	}

	/**
	 * Converts the given {@link InputStream} to a {@link String}.
	 * 
	 * @param aInputStream The {@link InputStream} to be converted.
	 * 
	 * @return The according {@link String}.
	 * 
	 * @throws IOException thrown in case conversion failed.
	 */
	protected String toString( InputStream aInputStream ) throws IOException {
		final ByteArrayOutputStream theBuffer = new ByteArrayOutputStream();
		int eRead;
		final byte[] theData = new byte[16384];
		while ( ( eRead = aInputStream.read( theData, 0, theData.length ) ) != -1 ) {
			theBuffer.write( theData, 0, eRead );
		}
		theBuffer.flush();
		return theBuffer.toString();
	}

	/**
	 * Converts the given {@link String} to an {@link InputStream}.
	 * 
	 * @param aString The {@link String} to be converted.
	 * 
	 * @return The according {@link InputStream}.
	 */
	protected InputStream toInputStream( String aString ) {
		return new ByteArrayInputStream( aString.getBytes( StandardCharsets.UTF_8 ) );
	}
}
