// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.refcodes.data.Encoding;
import org.refcodes.data.MarshalParameter;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;

/**
 * Implementation of the {@link CanonicalMapFactory} for parsing the HTML
 * notation.
 */
public class HtmlCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String BODY = "body";
	public static final String HEAD = "head";
	public static final String HTML = "html";
	public static final String[] ROOT_SELECTOR = { HTML, BODY };

	public static final String COMMENT_OPEN = "<!--";
	public static final String COMMENT_CLOSE = "-->";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		try {
			String theCharsetName = aProperties != null ? aProperties.get( MarshalParameter.CHARSET.getName() ) : null;
			if ( theCharsetName == null ) {
				theCharsetName = Encoding.UTF_8.getCode();
			}
			String theBaseUri = aProperties != null ? aProperties.get( MarshalParameter.BASE_URL.getName() ) : null;
			if ( theBaseUri == null ) {
				theBaseUri = "";
			}
			final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl( theDelimiter );
			final Parser theParser = Parser.htmlParser();
			theParser.settings( new ParseSettings( true, true ) );
			final Document theDocument = Jsoup.parse( aExternalRepresentation, theCharsetName, theBaseUri, theParser );
			final Elements theElements = theDocument.children();
			visitElements( theCanonicalMap, theElements, theCanonicalMap.getRootPath(), theDelimiter );
			postProcess( theCanonicalMap );
			return theCanonicalMap;
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from input stream <" + aExternalRepresentation + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder toUnmarshaled( String aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		try {
			String theBaseUri = null;
			if ( aProperties != null ) {
				theBaseUri = aProperties.get( MarshalParameter.BASE_URL.getName() );
			}
			if ( theBaseUri == null ) {
				theBaseUri = "";
			}
			final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl( theDelimiter );
			final Parser theParser = Parser.htmlParser();
			theParser.settings( new ParseSettings( true, true ) );
			final Document doc = Jsoup.parse( aExternalRepresentation, theBaseUri, theParser );
			final Elements theElements = doc.children();
			visitElements( theCanonicalMap, theElements, theCanonicalMap.getRootPath(), theDelimiter );
			postProcess( theCanonicalMap );
			return theCanonicalMap;
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation \"" + aExternalRepresentation + "\"!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return toMarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {

		// Auto-detect whether we need a root element |-->
		boolean isWellFormedHtml = false;
		Set<String> eDirs = aDataStructure.dirs();
		final Set<String> eLeaves = aDataStructure.leaves();
		if ( eDirs.size() == 1 && eLeaves.size() == 0 ) {
			final String theRoot = eDirs.iterator().next();
			if ( HTML.equalsIgnoreCase( theRoot ) ) {
				eDirs = aDataStructure.children( theRoot );
				if ( eDirs.size() >= 1 && eDirs.size() <= 2 ) {
					out: {
						for ( String eDir : eDirs ) {
							if ( !HEAD.equalsIgnoreCase( eDir ) && !BODY.equalsIgnoreCase( eDir ) ) {
								break out;
							}
						}
						isWellFormedHtml = true;
					}
				}
			}
		}
		if ( !isWellFormedHtml ) {
			aDataStructure = aDataStructure.retrieveTo( ROOT_SELECTOR );
		}
		// Auto-detect whether we need a root element <--|

		final String theXml = XmlCanonicalMapFactory.toXml( aDataStructure, DocumentProperty.hasArrayIndex( aProperties ) );

		// Write the comment |-->
		String theHead = "";
		final String theComment = DocumentProperty.toComment( aProperties );
		if ( theComment != null && theComment.length() > 0 ) {
			final String theLines[] = theComment.split( "\\r?\\n" );
			for ( String eLine : theLines ) {
				theHead += COMMENT_OPEN + " " + eLine + " " + COMMENT_CLOSE + System.lineSeparator();
			}
		}
		// Write the comment <--|

		return theHead + theXml;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return fromUnmarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final String theMarshaled = toMarshaled( aDataStructure, aProperties );
		final InputStream theMarshalledStream = new ByteArrayInputStream( theMarshaled.getBytes() );
		return theMarshalledStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void visitElements( CanonicalMapBuilder aCanonicalMap, Elements elements, String aPath, char aDelimiter ) {
		String eValue;
		String ePath;
		int next;
		for ( Element eElement : elements ) {
			ePath = aCanonicalMap.toPath( aPath, eElement.nodeName() );
			next = toAttributeIndex( eElement.attributes() );
			eValue = null;
			if ( eElement.hasText() ) {
				eValue = eElement.ownText();
			}
			if ( ( eValue == null || eValue.isEmpty() ) && eElement.val() != null && eElement.val().length() != 0 ) {
				eValue = eElement.val();
			}
			if ( ( eValue == null || eValue.isEmpty() ) && eElement.data() != null && eElement.data().length() != 0 ) {
				eValue = eElement.data();
			}
			if ( eValue != null && eValue.trim().isEmpty() ) {
				eValue = null;
			}
			if ( eValue != null ) {

				if ( next != -1 ) {
					aCanonicalMap.putValueAt( ePath, next, eValue );
				}
				else if ( aCanonicalMap.isArray( ePath ) ) {
					next = aCanonicalMap.nextArrayIndex( ePath );
					aCanonicalMap.putValueAt( ePath, next, eValue );
				}
				else if ( aCanonicalMap.containsKey( ePath ) ) {
					aCanonicalMap.putValueAt( ePath, 1, eValue );
					eValue = aCanonicalMap.remove( ePath );
					aCanonicalMap.putValueAt( ePath, 0, eValue );
				}
				else {
					aCanonicalMap.put( ePath, eValue );
				}
			}
			if ( next != -1 ) {
				ePath = aCanonicalMap.toPath( ePath, next );
			}
			visitAttributes( aCanonicalMap, eElement.attributes(), ePath );
			visitElements( aCanonicalMap, eElement.children(), ePath, aDelimiter );
		}
	}

	private int toAttributeIndex( Attributes aAttribs ) {
		final int index = -1;
		if ( aAttribs != null ) {
			try {
				for ( Attribute eAttrib : aAttribs ) {
					if ( INDEX_ATTRIBUTE.equals( eAttrib.getKey() ) ) {
						return Integer.parseInt( eAttrib.getValue() );
					}
				}
			}
			catch ( NumberFormatException ignore ) {/* ignore */ }
		}
		return index;
	}

	private void visitAttributes( CanonicalMapBuilder aCanonicalMap, Attributes aAttribs, String aPath ) {
		for ( Attribute eAttrib : aAttribs ) {

			if ( !INDEX_ATTRIBUTE.equals( eAttrib.getKey() ) ) {
				aCanonicalMap.put( new String[] { aPath, PathMap.ANNOTATOR + eAttrib.getKey() }, eAttrib.getValue() );
			}
		}
	}
}
