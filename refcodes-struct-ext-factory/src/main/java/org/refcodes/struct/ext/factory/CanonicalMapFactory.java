// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.stream.Collectors;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.factory.MarshalTypeFactory.MarshalTypeFactoryComposite;
import org.refcodes.factory.UnmarshalTypeFactory.UnmarshalTypeFactoryComposite;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;

/**
 * The {@link CanonicalMapFactory} creates data structures from external
 * representations and external representations from given data structures
 * (using notations for the external representations as of the actual
 * implementation of this interface).
 */
public interface CanonicalMapFactory extends UnmarshalTypeFactoryComposite<CanonicalMapBuilder, String, InputStream>, MarshalTypeFactoryComposite<CanonicalMap, String, InputStream> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMapBuilder toUnmarshaled( String aExternalRepresentation ) throws UnmarshalException {
		final InputStream theExternalRepresentation = new ByteArrayInputStream( aExternalRepresentation.getBytes() );
		return fromMarshaled( theExternalRepresentation );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMapBuilder toUnmarshaled( String aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		return toUnmarshaled( aExternalRepresentation );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		return toMarshaled( aDataStructure );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		final InputStream theExternalRepresentation = fromUnmarshaled( aDataStructure );
		try ( BufferedReader theBufferedReader = new BufferedReader( new InputStreamReader( theExternalRepresentation ) ) ) {
			return theBufferedReader.lines().collect( Collectors.joining( "\n" ) );
		}
		catch ( IOException e ) {
			throw new MarshalException( e.getMessage(), e );
		}
	}
}
