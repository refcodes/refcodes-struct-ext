// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * Implementation of the {@link CanonicalMapFactory} for parsing the JSON
 * notation.
 */
public class YamlCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final char COMMENT = '#';

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		final ObjectMapper theMapper = new ObjectMapper( new YAMLFactory() );
		theMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
		// theMapper.enable( SerializationFeature.WRITE_ENUMS_USING_TO_STRING );
		// theMapper.enable( DeserializationFeature.READ_ENUMS_USING_TO_STRING );
		try {
			final Map<Object, Object> theProperties = theMapper.readValue( aExternalRepresentation, new TypeReference<Map<Object, Object>>() {} );
			final CanonicalMapBuilder theResult = new CanonicalMapBuilderImpl( theProperties, theDelimiter );
			postProcess( theResult );
			return theResult;
		}
		catch ( JsonMappingException | JsonParseException e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return toMarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {

		final ObjectMapper theMapper = new ObjectMapper( new YAMLFactory() );
		theMapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
		// theMapper.enable( SerializationFeature.WRITE_ENUMS_USING_TO_STRING );
		// theMapper.enable( DeserializationFeature.READ_ENUMS_USING_TO_STRING );
		final Object theDataStructure = aDataStructure.toDataStructure();
		final String theYaml;
		try {
			theYaml = theMapper.writerWithDefaultPrettyPrinter().writeValueAsString( theDataStructure );

			// Write the comment |-->
			String theHead = "";
			final String theComment = DocumentProperty.toComment( aProperties );
			if ( theComment != null && theComment.length() > 0 ) {
				final String theLines[] = theComment.split( "\\r?\\n" );
				for ( String eLine : theLines ) {
					theHead += COMMENT + " " + eLine + System.lineSeparator();
				}
			}
			// Write the comment <--|

			return theHead + theYaml;
		}
		catch ( JsonProcessingException e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + CanonicalMap.class.getName() + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return fromUnmarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final String theMarshaled = toMarshaled( aDataStructure, aProperties );
		final InputStream theMarshalledStream = new ByteArrayInputStream( theMarshaled.getBytes() );
		return theMarshalledStream;
	}
}
