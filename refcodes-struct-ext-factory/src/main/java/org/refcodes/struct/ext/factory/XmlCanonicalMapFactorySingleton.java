// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

/**
 * The singleton of the {@link XmlCanonicalMapFactory} for easy
 * {@link CanonicalMapFactory} creation.
 */
public class XmlCanonicalMapFactorySingleton extends XmlCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static XmlCanonicalMapFactorySingleton _canonicalMapFactorySingleton;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link XmlCanonicalMapFactorySingleton} singleton.
	 */
	protected XmlCanonicalMapFactorySingleton() {}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link XmlCanonicalMapFactorySingleton}.
	 * 
	 * @return The {@link XmlCanonicalMapFactory} singleton's instance.
	 */
	public static XmlCanonicalMapFactory getInstance() {
		if ( _canonicalMapFactorySingleton == null ) {
			synchronized ( XmlCanonicalMapFactory.class ) {
				_canonicalMapFactorySingleton = new XmlCanonicalMapFactorySingleton();
			}
		}
		return _canonicalMapFactorySingleton;
	}
}
