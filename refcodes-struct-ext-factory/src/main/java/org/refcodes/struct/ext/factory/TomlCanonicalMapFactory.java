// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct.ext.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.refcodes.data.Delimiter;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathComparator;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

/**
 * Implementation of the {@link CanonicalMapFactory} interface with support of
 * so called "TOML properties". In addition, nested sections are supported: A
 * section represents the first portion of a path and looks like such: <code>
 * [SectionA]
 * ValueA=A
 * </code> This results in key/value property of: <code>
 * SectionA/ValueA=A
 * </code> <code>
 * [SectionA]
 * ValueA=A
 * 
 * [[SectionB]]
 * ValueB=B
 * </code> <code>
 * SectionA/ValueA=A
 * SectionA/SectionB/ValueB=B
 * </code> For TOML properties, see "https://en.wikipedia.org/wiki/TOML"
 */
public class TomlCanonicalMapFactory extends AbstractCanonicalMapFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final char SECTION_BEGIN = '[';
	public static final char SECTION_END = ']';
	public static final char[] COMMENTS = { '#', ';' };
	private static final String RESET_SECTION = "---";
	public static final char[] DELIMITERS = new char[] { '/', '.' };

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation ) throws UnmarshalException {
		return fromMarshaled( aExternalRepresentation, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, PathMap.DELIMITER );
		final char[] theSupportedDelimiters = DocumentProperty.toDelimiters( aProperties );

		try ( BufferedReader theReader = new BufferedReader( new InputStreamReader( aExternalRepresentation ) ) ) {
			String eLine;
			String eTruncated;
			final Stack<String> theSections = new Stack<>();
			Property eProperty;
			int eLineNum = 0;
			int eLevel;
			boolean isStartComment;
			final CanonicalMapBuilder theResult = new CanonicalMapBuilderImpl( theDelimiter );
			while ( theReader.ready() ) {
				eLineNum++;
				eLine = theReader.readLine();
				eTruncated = eLine.replaceAll( "^\\s+", "" );
				isStartComment = false;
				for ( char eChar : COMMENTS ) {
					if ( eTruncated.startsWith( "" + eChar ) ) {
						isStartComment = true;
						break;
					}
				}
				if ( !isStartComment ) {
					eTruncated = eLine.trim();
					if ( eTruncated.length() > 0 ) {

						eLevel = getLevel( eTruncated );

						// ---

						if ( eLevel > 0 ) {
							eTruncated = eTruncated.substring( eLevel, eTruncated.length() - eLevel );
							if ( theSupportedDelimiters != null && theSupportedDelimiters.length != 0 ) {
								for ( char eDelimiter : theSupportedDelimiters ) {
									if ( eDelimiter != theResult.getDelimiter() ) {
										eTruncated = eTruncated.replace( eDelimiter, theResult.getDelimiter() );
									}
								}
							}
							while ( theSections.size() >= eLevel ) {
								theSections.pop();
							}
							if ( theSections.size() + 1 < eLevel ) {
								throw new UnmarshalException( "The line '" + eLine + "' is of a wrong section nesting (number of opening '" + SECTION_BEGIN + "' and closing '" + SECTION_END + "' section identifiers) of <" + eLevel + ">, though expected a section nesting of <" + ( theSections.size() + 1 ) + ">", eLineNum );
							}
							theSections.push( eTruncated );
						}
						else if ( RESET_SECTION.equals( eTruncated ) ) {
							theSections.clear();
						}

						// ---

						else {

							if ( !eLine.contains( Delimiter.PROPERTY.getChar() + "" ) ) {
								throw new UnmarshalException( "Expected a '" + Delimiter.PROPERTY.getChar() + "' at line <" + eLineNum + ">, line cannot be parsed as property.", eLineNum );
							}
							eProperty = new PropertyImpl( eLine );

							if ( eProperty.getKey().contains( " " ) ) {
								throw new UnmarshalException( "The key \"" + eProperty.getKey() + "\" contains a space \" \" at line <" + eLineNum + ">, a key must not contain spaces.", eLineNum );
							}
							eTruncated = eProperty.getKey().trim();
							eTruncated = theResult.fromExternalPath( eTruncated, theSupportedDelimiters );
							theResult.put( theResult.toPath( toSectionsPath( theSections, theResult ), eTruncated ), eProperty.getValue() );
						}
					}
				}
			}
			return theResult;
		}
		catch ( IOException e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + ">!", e );
		}
		catch ( ParseException e ) {
			throw new UnmarshalException( "Unable to unmarshal the external representation from stream <" + aExternalRepresentation + "> at offset <" + e.getErrorOffset() + ">!", e.getErrorOffset(), e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return toMarshaled( aDataStructure, null );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final char theDelimiter = DocumentProperty.toDelimiter( aProperties, aDataStructure.getDelimiter() );
		final List<String> theGlobals = new ArrayList<>( aDataStructure.leaves() );
		final PathComparator thePathComparator = new PathComparator( aDataStructure.getDelimiter() );
		theGlobals.sort( thePathComparator );
		final List<String> theSections = new ArrayList<>( aDataStructure.dirs() );
		theSections.sort( thePathComparator );
		boolean hasWritten = false;
		String theResult = "";

		// Write the comment |-->
		final String theComment = DocumentProperty.toComment( aProperties );
		if ( theComment != null && theComment.length() > 0 ) {
			final String theLines[] = theComment.split( "\\r?\\n" );
			for ( String eLine : theLines ) {
				theResult += ( COMMENTS[0] + " " + eLine );
				theResult += System.lineSeparator();
			}
			hasWritten = true;
		}
		// Write the comment <--|

		String eExternalKey;

		// Write the global properties |-->
		if ( theGlobals != null && !theGlobals.isEmpty() ) {
			if ( hasWritten ) {
				theResult += System.lineSeparator();
			}
			for ( String eKey : theGlobals ) {
				eExternalKey = aDataStructure.toExternalPath( eKey, theDelimiter );
				theResult += ( eExternalKey + Delimiter.PROPERTY.getChar() + aDataStructure.get( eKey ) );
				theResult += System.lineSeparator();
			}
			hasWritten = true;
		}
		// Write the global properties <--|
		CanonicalMap eSecionProperties;
		List<String> eSectionKeys;
		for ( String eKey : theSections ) {

			if ( hasWritten ) {
				theResult += System.lineSeparator();
			}
			theResult += ( SECTION_BEGIN + eKey + SECTION_END );
			theResult += System.lineSeparator();
			theResult += System.lineSeparator();
			eSecionProperties = aDataStructure.retrieveFrom( eKey );
			eSectionKeys = new ArrayList<>( eSecionProperties.keySet() );
			eSectionKeys.sort( thePathComparator );
			for ( String eSectionKey : eSectionKeys ) {
				eExternalKey = aDataStructure.toExternalPath( eSectionKey, theDelimiter );
				eExternalKey = aDataStructure.toPropertyPath( eExternalKey );
				theResult += ( eExternalKey + Delimiter.PROPERTY.getChar() + eSecionProperties.get( eSectionKey ) );
				theResult += System.lineSeparator();
			}
			hasWritten = true;
		}
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure ) throws MarshalException {
		return fromUnmarshaled( aDataStructure, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromUnmarshaled( CanonicalMap aDataStructure, Map<String, String> aProperties ) throws MarshalException {
		final String theExternalRepresentation = toMarshaled( aDataStructure, aProperties );
		return toInputStream( theExternalRepresentation );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts the {@link Stack} to a path.
	 * 
	 * @param aSections The {@link Stack} representing the current sections
	 *        nesting.
	 * 
	 * @return The according path.
	 */
	private String toSectionsPath( Stack<String> aSections, CanonicalMap aDataStructure ) {
		final String[] theSections = aSections.toArray( new String[aSections.size()] );
		for ( int i = 0; i < theSections.length; i++ ) {
			theSections[i] = aDataStructure.fromExternalPath( theSections[i], DELIMITERS );
		}
		return aDataStructure.toPath( theSections );
	}

	/**
	 * Returns the hierarchical level of the sections. I.e. "[section]"
	 * represents a level of 1, "[[section]]" represents a level of two and
	 * "[[section]" results in a {@link ParseException}.
	 * 
	 * @throws ParseException Thrown in case the sections are not specified
	 *         correctly.
	 * 
	 * @param aLine The line for which to get the level.
	 * 
	 * @return A value > 0 represents the according level, a value of 0
	 *         represents no level at all.
	 */
	private int getLevel( String aLine ) throws ParseException {
		String theLine = aLine;
		int theLeft = 0;
		int theRight = 0;
		while ( theLine.startsWith( SECTION_BEGIN + "" ) ) {
			theLine = theLine.substring( 1 );
			theLeft++;
		}
		while ( theLine.endsWith( SECTION_END + "" ) ) {
			theLine = theLine.substring( 0, theLine.length() - 1 );
			theRight++;
		}
		if ( theLeft != theRight ) {
			throw new ParseException( "The line '" + aLine + "' starts with <" + theLeft + "> '" + SECTION_BEGIN + "' chars, though ends with <" + theRight + "> '" + SECTION_END + "' chars. It must start with as many '" + SECTION_BEGIN + "' chars as it ends with '" + SECTION_END + "' chars.", theLeft );
		}
		return theLeft;
	}
}
